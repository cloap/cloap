FROM index.alauda.cn/gugud/erlang:18.0

MAINTAINER suyu, suyu@gugud.com


ADD / /opt/cloap
WORKDIR /opt/cloap
RUN rebar clean && rebar get-deps && rebar compile && rebar generate

VOLUME ["/etc/gugud"] 
EXPOSE 7000
CMD /opt/cloap/rel/cloap/bin/cloap console
