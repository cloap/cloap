-module(rating_store).
-behaviour(gen_server).

-include_lib("eldap/include/eldap.hrl").

-record(state, {ldap, ratings}).

-define(ROOT_DN, "ou=ratings," ++ ldap_util:basedn()).
-define(RATING_DN(Id), "ou=" ++ Id ++ "," ++ ?ROOT_DN).

%%
%% 行政等级的数据持久层

-export([start_link/0]).
-export([init/1, terminate/2, code_change/3,
          handle_call/3, handle_cast/2, handle_info/2]).
-export([fetch/1, fetch_all/0, create/1, add_members/2, update/2, delete/1, name_existed/1, id_existed/1]).


%% 
%% OTP APIs
start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).


%% 
%% APIs
fetch_all() -> gen_server:call(?MODULE, fetch_all).

fetch(Id) when is_binary(Id) -> fetch(binary_to_list(Id));
fetch(Id)                    -> gen_server:call(?MODULE, {fetch, Id}).

id_existed(Id) when is_list(Id) -> id_existed(iolist_to_binary(Id)); 
id_existed(Id) when is_binary(Id) -> 
    gen_server:call(?MODULE, {id_existed, Id}).     % 查询 ID 是否在 LDAP 中已经存在

name_existed(Name) when is_binary(Name) -> 
    gen_server:call(?MODULE, {name_existed, Name}).  % 查询名称是否在 LDAP 中已经存在
create(Name) when is_binary(Name) -> 
    gen_server:call(?MODULE, {create, Name}).

update(Id, Name) when is_binary(Id) ->
    update(binary_to_list(Id), Name);
update(Id, Name) when is_list(Name) ->
    update(Id, list_to_binary(Name));
update(Id, Name) -> 
    gen_server:call(?MODULE, {update, Id, Name}).

delete(Id) when is_binary(Id) ->
    delete(binary_to_list(Id));
delete(Id) -> 
    gen_server:call(?MODULE, {delete, Id}).


add_members(Id, Members) ->
    case id_existed(Id) of 
        false -> gen_server:cast(?MODULE, {first_add_members, Id, Members});
        true  -> gen_server:cast(?MODULE, {next_add_members, Id, Members})
    end.


%% 
%% Callbacks
init([]) -> self() ! start_db, {ok, #state{ratings = []}}.

handle_call(fetch_all, _From, State = #state{ldap = Handle, ratings = Ratings}) ->
    Entries = ldap_util:search_tree(Handle, ?ROOT_DN, 
                                    [eldap:equalityMatch("objectClass", "groupOfNames")]),   
    LdapResult = [  {hd(proplists:get_value("ou", Attributes)), 
                        hd(proplists:get_value("cn", Attributes)), 
                        <<"durable">>} 
                    || #eldap_entry{attributes = Attributes} <- Entries ],
    {reply, LdapResult ++ [{K, V, <<"pending">>} || {K, V} <- Ratings], State};
handle_call({fetch, Id}, _From, State = #state{ldap = Handle, ratings = Ratings}) ->
    Ldap = search_rating(Handle, {id, Id}),
    Pend = [ {Key, Name} || {Key, Name} <- Ratings, Key == Id ],

    R   =   case {Ldap, Pend} of
                {[], []} -> {error, not_found};                 % 数据库与暂存表中均未找到
                {[], [{K,V}|_]} -> {K,V,<<"pending">>,[]};      % 只在暂存表中找到
                {[H|_],_} -> H#eldap_entry.attributes
            end,
    {reply, R, State};
handle_call({id_existed, Id}, _From, State = #state{ldap = Handle}) ->
    {reply, [] =/= search_rating(Handle, {id, Id}), State};
handle_call({name_existed, Name}, _From, State = #state{ldap = Handle}) ->
    {reply, [] =/= search_rating(Handle, {name, Name}), State};
handle_call({create, Name}, _From, State = #state{ldap = Handle, ratings = Ratings}) ->
    try 
        []     = search_rating(Handle, {name, Name}),
        []     = [ N || {_Id, N} <- Ratings, N == Name ],
        Rating = {cloap_util:uuidgen(), Name},
        {reply, Rating, State#state{ratings = [Rating | Ratings]}}
    catch
        error:{badmatch, _Other} ->
            {reply, {error, rating_exist}, State}
    end;
handle_call({update, Id, Name}, _From, State = #state{ldap = Handle, ratings = Ratings}) ->
    try
        LdapById    = search_rating(Handle, {id, Id}),
        LdapByName  = [hd(proplists:get_value("ou", Attr)) || #eldap_entry{attributes = Attr} <- search_rating(Handle, {name, Name})],
        PendById    = proplists:get_value(Id, Ratings, pend_not_found),
        PendByName  = [K || {K,V} <- Ratings, V == Name],

        R = case {LdapById, LdapByName, PendById, PendByName} of
                {[], _, pend_not_found, _} -> 
                    throw(not_found);
                {_, [Lid], pend_not_found, _} when Lid =/= Id ->  % 数据库中发现另外一个同名的账号
                    throw(rating_exist);   
                {[], _, _, [Pid]} when Pid =/= Id ->              % 暂定表中发现另外一个同名的账号
                    throw(rating_exist);   
                {_, _, pend_not_found, _} ->                      % 修改数据库中的信息
                    ldap;
                {[], _, _, _} ->
                    pend
        end,

        case R of 
            ldap -> 
                ok = eldap:modify(Handle, ?RATING_DN(Id), [ eldap:mod_replace("cn", [Name])]),
                {reply, {Id, Name}, State};
            pend ->
                {reply, {Id, Name}, State#state{ratings = [{Id, Name} | proplists:delete(Id, Ratings)]}}
        end
    catch
        throw:Reason -> 
            {reply, {error, Reason}, State}
        % error:Reason ->
        %     {reply, {error, Reason}, State}
    end;
handle_call({delete, Id}, _From, State = #state{ldap = Handle, ratings = Ratings}) ->
    try
        LdapById    = search_rating(Handle, {id, Id}),
        PendById    = proplists:get_value(Id, Ratings, pend_not_found),

        R = case {LdapById, PendById} of
                {[], pend_not_found} -> throw(not_found);
                {_, pend_not_found}  -> ldap;                     % 修改数据库中的信息
                {[], _}              -> pend
        end,

        case R of 
            ldap -> 
                ok = eldap:delete(Handle, ?RATING_DN(Id)),
                {reply, Id, State};
            pend ->
                {reply, Id, State#state{ratings = proplists:delete(Id, Ratings)}}
        end
    catch
        throw:Reason -> 
            {reply, {error, Reason}, State}
    end;
handle_call(_Action, _From, State)  ->  {reply, unknown_action, State}.



handle_cast({first_add_members, Id, Members}, State = #state{ldap = Handle, ratings = Ratings}) ->
    Name = proplists:get_value(Id, Ratings),
    ok = eldap:add(Handle, ?RATING_DN(Id), 
            [
                {"objectClass", ["groupOfNames"]},
                {"ou", [Id]},
                {"cn", [Name]},
                {"member", Members}
            ]),
    {noreply, State#state{ratings = proplists:delete(Id, Ratings)}};
handle_cast(stop, State)            ->  {stop, normal, State}.

handle_info(start_db, State) ->
    {ok, Handle} = ldap_util:start_db(),
    eldap:add(Handle, ?ROOT_DN, [
                                    {"ou", ["ratings"]}, 
                                    {"objectClass", ["organizationalUnit"]}
                                ]),
    {noreply, State#state{ldap = Handle}};
handle_info(_Action, State)         ->  {noreply, State}.

terminate(_Reason, _State)          ->  ok.
code_change(_OldVsn, State, _Extra) ->  {ok, State}.


% ----------------------------------------------------------------------------------------------------
% 私有函数
% ----------------------------------------------------------------------------------------------------
search_rating(Handle, {name, Name}) ->
    search_rating(Handle, "cn", Name);
search_rating(Handle, {id, Id}) ->
    search_rating(Handle, "ou", Id).
search_rating(Handle, Field, Value) ->
    ldap_util:search_tree(Handle, ?ROOT_DN, 
                            [eldap:equalityMatch(Field, Value), eldap:equalityMatch("objectClass", "groupOfNames")]).
