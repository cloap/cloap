-module(rating_api).
-behaviour(gen_server).
-include_lib("eldap/include/eldap.hrl").

% 等级服务的接口
-export([start_link/0]).
-export([init/1, terminate/2, code_change/3,
          handle_call/3, handle_cast/2, handle_info/2]).
-export([create_call/1, create_cast/1, update_call/2, delete_call/1, get/1]).

%% 
%% OTP APIs
start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% 同步调用与异步调用
create_call(Name) -> gen_server:call(?MODULE, {create, Name}).
create_cast(Name) -> gen_server:cast(?MODULE, {create, Name}).

update_call(Id, Name) -> gen_server:call(?MODULE, {update, Id, Name}).
delete_call(Id) -> gen_server:call(?MODULE, {delete, Id}).

get(Id) -> gen_server:call(?MODULE, {get, Id}).


%% 
%% Callbacks
init([]) -> {ok, #{}}.

handle_call({get, Id}, _From, State)  ->
    R = case rating_store:fetch(Id) of
            {error, Reason} -> 
                cloap_util:jsonize({failure, Reason, Id}, [result, reason, id]);
            Value when is_tuple(Value) -> 
                cloap_util:jsonize(Value, [id, name, state, members]);
            Value ->
                [{state, <<"durable">>} | 
                    cloap_util:jsonize(Value, [id, name, members], 
                                        {key, [{"ou", only_first}, {"cn", only_first}, {"member", keep}]})]
        end,
    {reply, R, State};




handle_call({create, Name}, _From, State)  ->
    R = case rating_store:create(Name) of
            {error, Reason} -> 
                cloap_util:jsonize({failure, Reason, Name}, [result, reason, name]);
            Value -> 
                cloap_util:jsonize(Value, [id, name])
        end,
    {reply, R, State};

handle_call({update, Id, Name}, _From, State)  ->
    R = case rating_store:update(Id, Name) of
            {error, Reason} -> 
                cloap_util:jsonize({failure, Reason, Id, Name}, [result, reason, id, name]);
            Value -> 
                cloap_util:jsonize(Value, [id, name])
        end,
    {reply, R, State};
handle_call({delete, Id}, _From, State)  ->
    R = case rating_store:delete(Id) of
            {error, Reason} -> 
                cloap_util:jsonize([failure, Reason, Id], [result, reason, id]);
            Value -> 
                cloap_util:jsonize([Value, success], [id, result])
        end,
    {reply, R, State};


handle_call(_Action, _From, State)  ->  {reply, unknown_action, State}.


handle_cast(stop, State)            ->  {stop, normal, State}.

handle_info(_Action, State)         ->  {noreply, State}.

terminate(_Reason, _State)          ->  ok.
code_change(_OldVsn, State, _Extra) ->  {ok, State}.
