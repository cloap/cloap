-module(acct_api).
-behaviour(gen_server).
-include_lib("eldap/include/eldap.hrl").

% 等级服务的接口
-export([start_link/0]).
-export([init/1, terminate/2, code_change/3,
          handle_call/3, handle_cast/2, handle_info/2]).
-export([create/1, create_cast/1, update_call/2, delete_call/1, get/1]).

%% 
%% OTP APIs
start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% 同步调用
create(Params) when is_map(Params) ->
    {ok, FirstName} = cloap_param:require(<<"cn">>, Params),
    {ok, SurName}   = cloap_param:require(<<"sn">>, Params),
    {ok, OrganId}   = cloap_param:require(<<"organ_id">>, Params),
    gen_server:call(?MODULE, {create, FirstName, SurName, OrganId, Params}).




create_cast(Name) -> gen_server:cast(?MODULE, {create, Name}).

update_call(Id, Params) -> gen_server:call(?MODULE, {update, Id, Params}).
delete_call(Id) -> gen_server:call(?MODULE, {delete, Id}).

get(Id) -> gen_server:call(?MODULE, {get, Id}).


%% 
%% Callbacks
init([]) -> {ok, #{}}.

handle_call({get, Id}, _From, State)  ->
    R = case acct_store:fetch(Id) of
            {error, Reason} -> 
                cloap_util:jsonize({failure, Reason, Id}, [result, reason, id]);
            Value -> Value
        end,
    {reply, R, State};



% -----------------------------------------------------------------------------
handle_call({create, FirstName, SurName, OrganId, Params}, _From, State)  ->
    R = case acct_store:create(FirstName, SurName, OrganId, Params) of
            [{error, Reason}, {id, Id}] -> 
                cloap_util:jsonize(cloap_util:failure(Reason, #{id => Id}));
            Value ->
                cloap_util:jsonize(Value)
        end,
    {reply, R, State};




handle_call({update, Id, Params}, _From, State)  ->
    R = case acct_store:update(Id, Params) of
            {error, Reason} -> 
                cloap_util:jsonize({failure, Reason, Id}, [result, reason, id]);
            Value -> 
                [{result, success}|Value]
        end,
    {reply, R, State};
handle_call({delete, Id}, _From, State)  ->
    R = case acct_store:delete(Id) of
            {error, Reason} -> 
                cloap_util:jsonize([failure, Reason, Id], [result, reason, id]);
            Value -> 
                cloap_util:jsonize([success, Value], [result, id])
        end,
    {reply, R, State};


handle_call(_Action, _From, State)  ->  {reply, unknown_action, State}.


handle_cast(stop, State)            ->  {stop, normal, State}.

handle_info(_Action, State)         ->  {noreply, State}.

terminate(_Reason, _State)          ->  ok.
code_change(_OldVsn, State, _Extra) ->  {ok, State}.
