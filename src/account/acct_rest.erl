-module(acct_rest).

% --------------------------------------------------------------------------------
% 行政级别的 Web 服务接口
% --------------------------------------------------------------------------------

-export([init/2, 
            allowed_methods/2, 
            content_types_provided/2, content_types_accepted/2,
            resource_exists/2, delete_resource/2]).
-export([search/2, modify/2]).




% --------------------------------------------------------------------------------
% 回调函数
% --------------------------------------------------------------------------------
init(Req, Opts) ->  
    {cowboy_rest, Req, Opts}.
allowed_methods(Req, State) ->
    {
        [<<"GET">>, <<"POST">>, <<"PUT">>, <<"DELETE">>, <<"HEAD">>, <<"OPTIONS">>],
        Req, State
    }.
content_types_provided(Req, State) ->
    {
        [
            {<<"application/json">>, search}
        ], 
        Req, State
    }.
content_types_accepted(Req, State) ->
    {
        [
            {<<"application/json">>, modify}
        ],
        Req, State
    }.
delete_resource(Req, State) ->
    Id = cowboy_req:binding(id, Req),
    R = acct_api:delete_call(Id),
    {true, cowboy_req:set_resp_body(jsx:encode(R), Req), State}.
resource_exists(Req, State) ->  
    {true, Req, State}.


% 查询 
search(Req, State) ->
    R   =   case cowboy_req:binding(id, Req) of
                undefined   -> acct_store:fetch_all();
                Id          -> acct_api:get(Id)
            end,
    {jsx:encode(R), Req, State}.


% 更新
modify(Req, State) ->
    Method = cowboy_req:method(Req),
    Id     = cowboy_req:binding(id, Req),
    {ok, Body, _Req}      = cowboy_req:body(Req),
    Params = jsx:decode(Body, [return_maps]),
    

    R   =   try {Method, Id} of
                {<<"POST">>, undefined} ->
                    cloap_util:with_param_check(fun acct_api:create/1, Params); 
                {<<"PUT">>, Id} -> 
                    acct_api:update_call(Id, Params)
            catch
                error:Reason -> [{error, Reason}]
            end,
    {true, cowboy_req:set_resp_body(jsx:encode(R), Req), State}.

