-module(acct_store).
-behaviour(gen_server).

-include_lib("eldap/include/eldap.hrl").

-record(state, {ldap}).

-define(ROOT_DN, "ou=people," ++ ldap_util:basedn()).
-define(USER_DN(Id), "uid=" ++ Id ++ "," ++ ?ROOT_DN).
-define(USER_CLASS, ["inetOrgPerson", "organizationalPerson", "person", "top"]).
-define(USER_PROPERTY, #{"uid" => <<"id">>, "cn" => <<"cn">>, "sn" => <<"sn">>, 
                            "displayName" => <<"display_name">>, "userPassword" => <<"password">>,
                            "mail" => <<"mail">>, "mobile" => <<"mobile">>}).
-define(USER_PASSWORD_SIZE, 6).
-define(NO_PARENT, <<>>).

%%
%% 行政等级的数据持久层

-export([start_link/0]).
-export([init/1, terminate/2, code_change/3,
          handle_call/3, handle_cast/2, handle_info/2]).
-export([fetch/1, fetch_all/0, create/4, add_members/2, update/2, delete/1, name_existed/1, id_existed/1, search_user/2, entry_to_map/1]).


%% 
%% OTP APIs
start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).


%% 
%% APIs
fetch_all() -> gen_server:call(?MODULE, fetch_all).

fetch(Id) when is_binary(Id) -> fetch(binary_to_list(Id));
fetch(Id)                    -> gen_server:call(?MODULE, {fetch, Id}).

id_existed(Id) when is_list(Id) -> id_existed(iolist_to_binary(Id)); 
id_existed(Id) when is_binary(Id) -> 
    gen_server:call(?MODULE, {id_existed, Id}).     % 查询 ID 是否在 LDAP 中已经存在

name_existed(Name) when is_binary(Name) -> 
    gen_server:call(?MODULE, {name_existed, Name}).  % 查询名称是否在 LDAP 中已经存在
create(FirstName, SurName, OrganId, Params) when is_map(Params) -> 
    gen_server:call(?MODULE, {create, FirstName, SurName, OrganId, Params}).

update(Id, Params) when is_binary(Id) ->
    update(binary_to_list(Id), Params);
update(Id, Params) -> 
    gen_server:call(?MODULE, {update, Id, Params}).

delete(Id) when is_binary(Id) ->
    delete(binary_to_list(Id));
delete(Id) -> 
    gen_server:call(?MODULE, {delete, Id}).


add_members(Id, Members) ->
    case id_existed(Id) of 
        false -> gen_server:cast(?MODULE, {first_add_members, Id, Members});
        true  -> gen_server:cast(?MODULE, {next_add_members, Id, Members})
    end.


%% 
%% Callbacks
init([]) -> self() ! start_db, {ok, #state{}}.

handle_call(fetch_all, _From, State = #state{ldap = Handle}) ->
    Entries = ldap_util:search_tree(Handle, ?ROOT_DN, 
                                    [eldap:equalityMatch("objectClass", X) || X <- ?USER_CLASS]),
    Result = [  cloap_util:binarize(maps:from_list(translate(maps:remove("userPassword", ?USER_PROPERTY), Attributes))) 
                        || #eldap_entry{attributes = Attributes} <- Entries ],
    {reply, Result, State};
handle_call({fetch, Id}, _From, State = #state{ldap = Handle}) ->
    try
        [User|_] = cloap_util:demine([], not_found, search_user(Handle, {id, Id})),
        Result = translate(maps:remove("userPassword", ?USER_PROPERTY), User#eldap_entry.attributes),
        {reply, Result, State}
    catch
        _:Reason ->
            {reply, {error, Reason}, State}
    end;
handle_call({id_existed, Id}, _From, State = #state{ldap = Handle}) ->
    {reply, [] =/= search_user(Handle, {id, Id}), State};



handle_call({create, FirstName, SurName, OrganId, #{<<"id">> := Id} = Params}, From, State) ->
    handle_call({create, FirstName, SurName, OrganId, Id, Params}, From, State);
handle_call({create, FirstName, SurName, OrganId, Params}, From, State) ->
    handle_call({create, FirstName, SurName, OrganId, cloap_util:uuidgen(), Params}, From, State);
handle_call({create, FirstName, SurName, OrganId, Id, Params}, From, State) when is_binary(Id) ->
    handle_call({create, FirstName, SurName, OrganId, binary_to_list(Id), Params}, From, State);
handle_call({create, FirstName, SurName, OrganId, Id, Params}, _From, State = #state{ldap = Ldap}) ->
    Resp = try
        ability(user_exist, Id, Ldap),
        ability(organ_exist, OrganId),
        {ok, Params1} = cloap_util:lost_and_set(<<"display_name">>, fun() -> iolist_to_binary([SurName, FirstName]) end, Params),
        {ok, Params2} = cloap_util:lost_and_set(<<"password">>, fun() -> cloap_util:random_string(?USER_PASSWORD_SIZE) end, Params1),
        ok = eldap:add(Ldap, ?USER_DN(Id), [{"objectClass", ?USER_CLASS} | properties(?USER_PROPERTY, Params2)]),
        organ_store:add_members(OrganId, ?USER_DN(Id)),
        maps:without([<<"password">>], Params2)
    catch
        throw:Reason -> cloap_util:failure(Reason, Params)
    end,
    {reply, Resp, State};


handle_call({update, Id, Params}, _From, State = #state{ldap = Handle}) ->
    try
        [User|_] = cloap_util:demine([], not_found, search_user(Handle, {id, Id})),
        Commands =  [ 
                        case proplists:get_value(K, User#eldap_entry.attributes) of
                            undefined -> eldap:mod_add(K, V);
                            _         -> eldap:mod_replace(K, V)
                        end
                            || {K, V} <- properties(?USER_PROPERTY, Params)
                    ],
        ok = eldap:modify(Handle, ?USER_DN(Id), Commands),
        [User1|_] = search_user(Handle, {id, Id}),
        Result = translate(maps:remove("userPassword", ?USER_PROPERTY), User1#eldap_entry.attributes),
        {reply, Result, State}
    catch
        _:Reason -> 
            {reply, {error, Reason}, State}
    end;

handle_call({delete, Id}, From, State) when is_binary(Id) ->
    handle_call({delete, binary_to_list(Id)}, From, State);
handle_call({delete, Id}, _From, State = #state{ldap = Handle}) ->
    try
        cloap_util:demine([], not_found, search_user(Handle, {id, Id})),
        ok = eldap:delete(Handle, ?USER_DN(Id)),
        {reply, Id, State}
    catch
        _:Reason ->
            {reply, {error, Reason}, State}
    end;

handle_call(_Action, _From, State)  ->  {reply, unknown_action, State}.



handle_cast(stop, State)            ->  {stop, normal, State}.

handle_info(start_db, State) ->
    {ok, Handle} = ldap_util:start_db(),
    eldap:add(Handle, ?ROOT_DN, [
                                    {"ou", ["people"]}, 
                                    {"objectClass", ["organizationalUnit"]}
                                ]),
    {noreply, State#state{ldap = Handle}};
handle_info(_Action, State)         ->  {noreply, State}.

terminate(_Reason, _State)          ->  ok.
code_change(_OldVsn, State, _Extra) ->  {ok, State}.


% ----------------------------------------------------------------------------------------------------
% 私有函数
% ----------------------------------------------------------------------------------------------------
ability(organ_exist, OrganId) ->
    cloap_util:demine(fun(X) -> success =:= maps:get(result, X, success) end, organ_not_exist, organ_api:get(OrganId)).
ability(user_exist, Id, Ldap) ->
    cloap_util:demine(fun(X) -> [] =:= X end, user_exist, search_user(Ldap, {id, Id})).




search_user(Handle, {id, Id}) ->
    search_user(Handle, "uid", Id).
search_user(Handle, Field, Value) ->
    ldap_util:search_tree(Handle, ?ROOT_DN, 
                            [eldap:equalityMatch(Field, Value) | lists:map(fun(X) -> eldap:equalityMatch("objectClass", X) end, ?USER_CLASS)]).


translate(Property, Params) when is_map(Property) ->
    translate(maps:to_list(Property), Params, []).
translate([], _Params, Result) ->
    Result;
translate([{K, V}|T], Params, Result) ->
    Pairs = case proplists:get_value(K, Params) of
                undefined -> Result;
                Value     -> [{V, cloap_util:binarize(hd(Value))} | Result]
            end,
    translate(T, Params, Pairs).



properties(Property, Params) when is_map(Property), is_map(Params) ->
    properties(maps:to_list(Property), Params, []).
properties([], _Params, Result) ->
    Result;
properties([{K, V}|T], Params, Result) ->
    Pairs = case maps:is_key(V, Params) of
                true  -> [{K, [maps:get(V, Params)]} | Result];
                false -> Result
            end,
    properties(T, Params, Pairs).



entry_to_map(Entry) when is_record(Entry, eldap_entry) ->
    maps:from_list(translate(maps:remove("userPassword", ?USER_PROPERTY), Entry#eldap_entry.attributes)).
