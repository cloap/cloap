-module(cloap_config).
-behaviour(gen_server).
-include("cloap.hrl").

%%
%% 应用模块管理
-export([start_link/0]).
-export([init/1, terminate/2, code_change/3,
          handle_call/3, handle_cast/2, handle_info/2]).
-export([fetch_all/0, update/1]).

-record(state, {riak}).
-define(BUCKET, <<"cloap.system.configs">>).   % Riak 数据表名

%%
%% OTP APIs
start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%%
%% APIs
fetch_all()                         -> gen_server:call(?MODULE, fetch_all).

update(Params) when is_list(Params) -> update(maps:from_list(Params));
update(Params) when is_map(Params)  -> gen_server:cast(?MODULE, {update, Params}).

%%
%% Callbacks
init([]) ->
  self() ! start_riak,
  {ok, #state{}}.

handle_call(fetch_all, _From, S = #state{riak = Riak}) ->
  Result = ggd_riak:get_all(Riak, ?BUCKET),
  {reply, [#{key => K, value => V} || {K, V} <- Result], S};
handle_call(_Action, _From, S) -> {noreply, S}.

handle_cast({update, Params}, S = #state{riak = Riak}) ->
  Result = maps:map(fun(K, V) -> ggd_riak:set(Riak, ?BUCKET, K, V), V end, Params),
  cloap_event:notify({'sysconfig.update', Result}),
  {noreply, S};
handle_cast(stop, State) -> {stop, normal, State}.

handle_info(start_riak, _State) ->
  {ok, Pid} = ggd_riak:start(),
  {noreply, #state{riak = Pid}};
handle_info(_Action, S) -> {noreply, S}.

terminate(_Reason, _State) -> ok.
code_change(_OldVsn, S, _Extra) -> {ok, S}.