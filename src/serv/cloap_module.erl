-module(cloap_module).
-behaviour(gen_server).
-include("cloap.hrl").

%%
%% 应用模块管理
-export([start_link/0]).
-export([init/1, terminate/2, code_change/3,
          handle_call/3, handle_cast/2, handle_info/2]).
-export([register/2]).

-record(state, {riak}).
-define(BUCKET, <<"cloap.modules">>).   % Riak 数据表名

%% 
%% OTP APIs
start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%%
%% APIs
register(Id, Params) -> gen_server:cast(?MODULE, {register, Id, Params}).


%% 
%% Callbacks
init([]) -> 
  {ok, #state{}}.    

handle_call(_Action, _From, S) ->
  {ok, [], S}.

handle_cast({register, _Id, _Params}, S) ->
  {noreply, S};
handle_cast(stop, State) ->
  {stop, normal, State}.

handle_info(_Action, S) ->
  {noreply, S}.

terminate(_Reason, _State) ->
  ok.
code_change(_OldVsn, S, _Extra) ->
  {ok, S}.