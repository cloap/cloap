-module(cloap_event_websocket).
-behaviour(gen_event).
-export([add_handler/0, delete_handler/0]).
-export([init/1, handle_event/2, handle_call/2, 
         handle_info/2, code_change/3, terminate/2]).

%% 
%% 接口函数
add_handler()       -> cloap_event:add_handler(?MODULE, []).
delete_handler()    -> cloap_event:delete_handler(?MODULE, []).


%% 
%% 回调函数
init([])                            -> {ok, #{}}.
handle_call(_Request, State)        -> {ok, ok, State}.
handle_info(_Info, State)           -> {ok, State}.
terminate(_Reason, _State)          -> ok.
code_change(_OldVsn, State, _Extra) -> {ok, State}.


%%
%% 事件处理
handle_event({'sysconfig.update', Configure}, State) when is_map(Configure) -> 
  cloap_handler:broadcast('sysconfig.update', Configure),
  {ok, State};
handle_event(_UnkownMessage, State) -> {ok, State}.

