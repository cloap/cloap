-module(ggd_json).
-include("cloap.hrl").

-export([result/1]).

result({error, Reason}) when is_tuple(Reason)  -> [{error, [Reason]}];
result({error, Reason}) when is_atom(Reason); is_list(Reason); is_binary(Reason) -> [{error, Reason}];
result(Value) when is_list(Value); is_map(Value) -> Value.

