-module(cloap_util).
-define(RANDOM_CHARS, "01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz").
-export([demine/3, uuidgen/0, uuidgen/1, jsonize/1, jsonize/2, jsonize/3, binarize/1, random_string/1, 
            ok/1, ok/2, success/1, failure/2, 
            lost_and_set/3,
            max_number/1, to_integer/1, format_date/1, unique/1,
            with_param_check/2]).


with_param_check(Fun, Params) when is_function(Fun), is_map(Params) ->
    try 
        Fun(Params)
    catch
        error:{badmatch, {not_found, Key}} ->
            cloap_util:failure(#{lost_param => Key}, Params);
        error:Reason ->
            throw(Reason)
    end.


% -----------------------------------------------------------------------------
-spec lost_and_set(Key :: term(), Fun :: function(), Orig :: map()) -> {ok, map()}.
lost_and_set(Key, Fun, Orig) 
    when is_function(Fun), is_map(Orig) ->
    case maps:is_key(Key, Orig) of
        true    -> {ok, Orig};
        false   -> {ok, Orig#{Key => Fun()}}
    end.




% 返回 {ok, Normal} 或者 error 信息
ok(Value) ->
    ok(Value, []).

ok(Value, Options) when is_list(Options) ->
    ok_format(proplists:get_value(Value, Options), Value).
ok_format(undefined, Value) ->
    {ok, Value};
ok_format(Error, _Value) ->
    Error.



% 检查生成结果，如果命名关注错误，则并抛出希望的错误信息
demine(Mine, Throw, {Mod, Fun, Args}) when is_list(Args) ->
    demine(Mine, Throw, apply(Mod, Fun, Args));
demine(Mine, Throw, Result) when is_function(Mine) ->
    demine(Mine(Result), Throw, false);
demine(Mine, Throw, Result)  ->
    case Result of
        Mine -> throw(Throw);
        _Other -> Result
    end.



% 生成 UUID
uuidgen() -> 
    uuid:to_string(uuid:uuid4()).
uuidgen(binary) ->
    list_to_binary(uuidgen()).

% 转换为 JSON 转换识别的格式
jsonize(Value) when is_map(Value) ->
    maps:map(fun(_K, V) -> binarize(V) end, Value).
jsonize(Values, Fields) when is_tuple(Values) -> 
    jsonize(tuple_to_list(Values), Fields);
jsonize([], _Fields) ->
    [];
jsonize(Values, Fields) when is_list(Values) ->
    lists:zip(Fields, [binarize(V) || V <- Values]).
jsonize(Values, Fields, {key, Keys}) ->
    Fun = fun(Val, Act) -> 
            case Act of
                only_first  -> hd(Val);
                keep        -> Val
            end
          end,

    lists:zip(Fields, [binarize(Fun(proplists:get_value(K, Values), Action)) || {K, Action} <- Keys]).


% 转换为二进制格式，只转换字符串或者字符串数组
binarize_map(Map) when is_map(Map) ->
    maps:map(fun(_K,V) -> binarize_map(binarize(V)) end, Map);
binarize_map(Map) ->
    binarize(Map).



binarize(Value) when    is_number(Value); 
                        is_boolean(Value); 
                        is_atom(Value); 
                        is_binary(Value) -> Value;
binarize([]) -> [];
binarize([H|T]) when is_list(H) -> 
    binarize([H|T], []);
binarize(H) when is_map(H) ->
    binarize_map(H);
binarize(H) when is_list(H) ->
    iolist_to_binary(H).

binarize([], Result) -> Result;
binarize(Value, Result) when is_list(Value) ->
    case Value of
        [H|T] when is_list(H) -> binarize(H, binarize(T, Result));
        V -> [iolist_to_binary(V) | Result]
    end.


% 成功与失败的格式化
success(Orgin) when is_map(Orgin) ->
    Orgin#{result => success};
success(Orgin) when is_list(Orgin) ->
    [{result, success} | Orgin].

failure(Reason, Orgin) when is_map(Orgin) ->
    Orgin#{result => failure, reason => Reason};
failure(Reason, Orgin) when is_list(Orgin) ->
    [{result, failure}, {reason, Reason} | Orgin].


% 最大的数字
max_number([]) -> 0;
max_number(Lists) -> lists:max(Lists).

% 转换为整数
to_integer(V) when is_binary(V) ->
    binary_to_integer(V);
to_integer([]) ->
    [];
to_integer([H|T]) when is_binary(H) ->
    [binary_to_integer(H) | to_integer(T)];
to_integer([H|T]) when is_list(H) ->
    [list_to_integer(H) | to_integer(T)];
to_integer([H|T]) when is_integer(H) ->
    [H | to_integer(T)].


% 日期格式化
format_date(Time) when is_integer(Time) ->
    format_date(calendar:gregorian_seconds_to_datetime(Time));
format_date({{Year, Month, Day}, {Hour, Minute, Second}}) ->
    iolist_to_binary(io_lib:format("~4..0w-~2..0w-~2..0w ~2..0w:~2..0w:~2..0w", [Year, Month, Day, Hour, Minute, Second])).

% 数组元素唯一化
unique(List) when is_list(List) ->
    gb_sets:to_list(gb_sets:from_list(List)).

% 生成随机字符串
random_string(Size) ->
    random:seed(erlang:monotonic_time()),
    random_string(Size, ?RANDOM_CHARS, length(?RANDOM_CHARS), "").
random_string(0, _Chars, _Scope, Result) ->
    Result;
random_string(Seq, Chars, Scope, Result) when Seq > 0 ->
    random_string(Seq - 1, Chars, Scope,
                    [lists:nth(random:uniform(Scope), Chars) | Result]).
