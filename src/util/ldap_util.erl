-module(ldap_util).
-include_lib("eldap/include/eldap.hrl").

-export([start_db/0, basedn/0, search_tree/3, 
            get/2, get_value/2, get_values/2, get_members/2, member_of/3]).



% 【GET】获得节点属性
% ------------------------------------------------------------
-spec get(Dn :: string(), Ldap :: pid()) -> {ok, #eldap_entry{}} | not_found.
get(Dn, Ldap) when is_binary(Dn) ->
    get(binary_to_list(Dn), Ldap);
get(Dn, Ldap) ->
    try 
        {ok, #eldap_search_result{entries = [Entry]}} = 
            eldap:search(Ldap, [{base, Dn}, {scope, eldap:baseObject()}, {filter, eldap:'and'([])}]),
        {ok, Entry}
    catch
        error:{badmatch,{error,noSuchObject}} -> not_found
    end.





% 【GET_MEMBERS】获得成员列表
% ------------------------------------------------------------
-spec get_members(Dn :: string(), Ldap :: pid()) -> {ok, [#eldap_entry{}]}.
get_members(Dn, Ldap) ->
    case get(Dn, Ldap) of
        {ok, Entry} ->
            {ok, proplists:get_value("member", Entry#eldap_entry.attributes)};
        not_found ->
            {ok, []}
    end.


% 【MEMBER_OF】寻找将节点纳入成员的节点
% ------------------------------------------------------------
-spec member_of(Dn :: string() | binary(), BaseDn :: string(), Ldap :: pid()) -> {ok, #eldap_entry{}}.
member_of(Dn, BaseDn, Ldap) ->
    {ok, #eldap_search_result{entries = Entries}} = 
            eldap:search(Ldap, [{base, BaseDn}, 
                {scope, eldap:wholeSubtree()}, 
                {filter, eldap:'and'([eldap:equalityMatch("member", Dn)])}]),
    {ok, Entries}.    




% ------------------------------------------------------------
% 【FUN】获取属性值
% ------------------------------------------------------------
get_value({Key, array}, Entry) when is_atom(Key) ->
    get_value({atom_to_list(Key), array}, Entry);
get_value({Key, array}, Entry) when is_record(Entry, eldap_entry) ->
    Attr = Entry#eldap_entry.attributes,
    cloap_util:binarize(proplists:get_value(Key, Attr));
get_value(Key, Entry) when is_atom(Key) ->
    get_value(atom_to_list(Key), Entry);
get_value(Key, Entry) ->
    hd(get_value({Key, array}, Entry)).

get_values([], _Entry) -> 
    [];
get_values([H|T], Entry) ->
    [get_value(H, Entry) | get_values(T, Entry)].
    



start_db() ->
    Host    = os:getenv("CLOAP_LDAP_HOST", "127.0.0.1"),
    Port    = list_to_integer(os:getenv("CLOAP_LDAP_PORT", "389")),
    BindDn  = os:getenv("CLOAP_LDAP_BINDDN", "cn=admin,dc=gugud,dc=com"),
    Passwd  = os:getenv("CLOAP_LDAP_PASSWORD", "passwd.gugud.com"),

    {ok, Handle} = eldap:open([Host], [{port, Port}]),
    ok = eldap:simple_bind(Handle, BindDn, Passwd),
    {ok, Handle}.

basedn() ->
    BindDn  = os:getenv("CLOAP_LDAP_BINDDN", "cn=admin,dc=gugud,dc=com"),
    string:substr(BindDn, length("cn=admin,") + 1).


% -----------------------------------------------------------------------------
% 查找
% -----------------------------------------------------------------------------
search_tree(Handle, BaseDn, Filters) ->
    {ok, #eldap_search_result{entries = Entries}} = 
        eldap:search(Handle, 
                    [{base, BaseDn}, 
                        {filter, eldap:'and'(Filters)},
                        {scope, eldap:wholeSubtree()}]),
    Entries.
