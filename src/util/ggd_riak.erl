-module(ggd_riak).
-export([start/0, get/3, get/4, get_all/2, set/4, update/4, remove/3, remove_all/2, key_exist/3]).


start() ->
  riakc_pb_socket:start_link("riak", 8087, [auto_reconnect, keepalive]).


%%
%% @doc 获取特定 Key 的数值，如果未找到，默认返回 not_found()
get(Pid, Bucket, Key) -> get(Pid, Bucket, Key, ggd_common:not_found(Key)).

%%
%% @doc 获取特定 Key 的数值，如果未找到，返回默认数值
get(Pid, Bucket, Key, DefVal) ->
  case riakc_pb_socket:get(Pid, Bucket, Key) of
    {ok, Fetched} -> to_term(riakc_obj:get_value(Fetched));
    {error, notfound} -> DefVal
  end.


%%
%% @doc 返回全部数值
get_all(Pid, Bucket) ->
  {ok, Keys} = riakc_pb_socket:list_keys(Pid, Bucket),
  [{Key, get(Pid, Bucket, Key)} || Key <- Keys].



key_exist(Pid, Bucket, Key) ->
  case riakc_pb_socket:get(Pid, Bucket, Key) of
    {ok, _Fetched}    -> true;
    {error, notfound} -> false
  end.




remove(Pid, Bucket, Key) ->
  case riakc_pb_socket:get(Pid, Bucket, Key) of
    {ok, _Fetched}      -> riakc_pb_socket:delete(Pid, Bucket, Key);
    {error, notfound}   -> ok
  end.


remove_all(Pid, Bucket) ->
  {ok, Keys} = riakc_pb_socket:list_keys(Pid, Bucket),
  [riakc_pb_socket:delete(Pid, Bucket, Key) || Key <- Keys].


set(Pid, Bucket, Key, Value) ->
  Obj = case riakc_pb_socket:get(Pid, Bucket, Key) of
          {ok, Fetched} ->
            riakc_obj:update_value(Fetched, Value);
          {error, notfound} ->
            riakc_obj:new(Bucket, Key, Value)
        end,            
  {ok, NewObj} = riakc_pb_socket:put(Pid, Obj, [return_body]), 
  NewObj.

update(Pid, Bucket, Key, Value) ->
  case riakc_pb_socket:get(Pid, Bucket, Key) of
    {error, notfound} -> {error, notfound};
    _Obj -> set(Pid, Bucket, Key, Value)
  end.

%% @private
to_term(Value) ->
  try binary_to_term(Value) of
    Val -> Val
  catch 
    _:_ -> Value
  end.

