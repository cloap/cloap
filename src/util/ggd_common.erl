-module(ggd_common).
-export([not_found/1, uuid/0, timestamp/0, replace/2, success/0, fail/1]).

not_found(Key) -> {error, [{not_found, Key}]}.

%%
%% 生成随机字符串
uuid() -> list_to_binary(uuid:to_string(uuid:uuid4())).

%%
%% 当前时间戳
timestamp() -> calendar:datetime_to_gregorian_seconds(calendar:local_time()).


replace([], Array) -> Array;
replace([{K, V}|T], Array) -> 
  replace(T, [{K, V} | proplists:delete(K, Array)]).


success() -> [{result, ok}].
fail(Reason) -> [{result, fail}, {reason, Reason}].