-module(cloap_param).
-export([require/2]).



% -----------------------------------------------------------------------------
% 参数必须存在
% -----------------------------------------------------------------------------
% -spec require(Key :: term(), Params :: map()) -> {ok, Param :: term()} | {not_found, Key :: term()}.
require(Key, Params) when is_map(Params) ->
    require_key(Key, maps:get(Key, Params, undefined)).
require_key(Key, undefined) ->
    {not_found, Key};
require_key(_Key, Value) ->
    {ok, Value}.
