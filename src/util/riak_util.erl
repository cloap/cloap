-module(riak_util).

-export([start_db/0, add_index_value/2, search_index/4,
            remove_all_db/1, remove_db/2, remove/3,
            get_object/3, get_value/1, get_value/3, get_values/2, get_index_values/2, get_index_values/3, get_keys/2,
            set_value/4]).


% 启动数据库连接
start_db() ->
  riakc_pb_socket:start_link("riak", 8087, [auto_reconnect, keepalive]).


% 增加索引值
add_index_value(notfound, Value) -> [Value];
add_index_value(List, Value)     -> [Value | List].


% 删除数据库
remove_all_db(Pid) ->
    {ok, Buckets} = riakc_pb_socket:list_buckets(Pid),
    [ remove_db(Pid, Bucket) || Bucket <- Buckets ],
    ok.
remove_db(Pid, Bucket) ->
    {ok, Keys} = get_keys(Pid, Bucket),
    [ riakc_pb_socket:delete(Pid, Bucket, Key) || Key <- Keys ],
    ok.

remove(Pid, Bucket, Key) ->
    riakc_pb_socket:delete(Pid, Bucket, Key).


% 获取数据对象
get_object(Pid, Bucket, Key) when is_integer(Key) ->
    get_object(Pid, Bucket, integer_to_binary(Key));
get_object(Pid, Bucket, Key) ->
    get_object(riakc_pb_socket:get(Pid, Bucket, Key)).

get_object({ok, Object}) ->
    Object;
get_object({error, notfound}) ->
    not_found. 


% 获取数据对象的值
get_value(Atom) when is_atom(Atom) ->
    Atom;
get_value(Object) ->
    Value = riakc_obj:get_value(Object),
    try binary_to_term(Value) of
        Term -> Term
    catch 
        _:_ -> Value
    end.
get_value(Pid, Bucket, Key) ->
    get_value(get_object(Pid, Bucket, Key)).

get_values(Pid, Bucket) ->
    {ok, Keys} = get_keys(Pid, Bucket),
    [get_value(Pid, Bucket, K) || K <- Keys].


% 获取数据索引值
get_index_values(Meta, Index) ->
    get_index_values(Meta, Index, []).
get_index_values(Meta, Index, Options) ->
    Values = get_index_values(riakc_obj:get_secondary_index(Meta, Index)),
    get_index_first_value(lists:member(first, Options), Values).

get_index_values(notfound) ->
    [];
get_index_values(List) ->
    List.

get_index_first_value(true, List) ->
    hd(List);
get_index_first_value(false, List) ->
    List.


% 获取数据关键值
get_keys(Pid, Bucket) ->
    riakc_pb_socket:list_keys(Pid, Bucket).


% 查找索引数据
search_index(Pid, Bucket, Index, Key) ->
    {ok, {_Index, Values, _, _}} = riakc_pb_socket:get_index(Pid, Bucket, Index, Key),
    {ok, Values}.

% 编辑数据值
set_value(Pid, Bucket, Key, Value) ->
  Obj = case riakc_pb_socket:get(Pid, Bucket, Key) of
          {ok, Fetched} ->
            riakc_obj:update_value(Fetched, Value);
          {error, notfound} ->
            riakc_obj:new(Bucket, Key, Value)
        end,            
  riakc_pb_socket:put(Pid, Obj, [return_body]).

