-module(organ_api).
-behaviour(gen_server).
-include_lib("eldap/include/eldap.hrl").

% 等级服务的接口
-export([start_link/0]).
-export([init/1, terminate/2, code_change/3,
          handle_call/3, handle_cast/2, handle_info/2]).
-export([create_call/2, create_cast/1, update_call/2, delete_call/1, get/1, tree/0]).

%% 
%% OTP APIs
start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% 同步调用与异步调用
create_call(Name, Parent) -> gen_server:call(?MODULE, {create, Name, Parent}).
create_cast(Name) -> gen_server:cast(?MODULE, {create, Name}).

update_call(Id, Name) -> gen_server:call(?MODULE, {update, Id, Name}).
delete_call(Id) -> gen_server:call(?MODULE, {delete, Id}).

get(Id) -> gen_server:call(?MODULE, {get, Id}).


% 按隶属关系生成组织机构关系树
% --------------------------------------------------------------------------------
-spec tree() -> {ok, [map()]}.
tree() ->
    Resp = organ_store:fetch_all(),

    {ok, Resp}.




%% 
%% Callbacks
init([]) -> {ok, #{}}.

handle_call({get, Id}, _From, State)  ->
    {reply, organ_store:fetch(Id), State};
handle_call({create, Name, Parent}, _From, State)  ->
    R = organ_store:create(Name, Parent),
    {reply, cloap_util:jsonize(R), State};

handle_call({update, Id, Name}, _From, State)  ->
    {reply, organ_store:update(Id, Name), State};
handle_call({delete, Id}, _From, State)  ->
    {reply, organ_store:delete(Id), State};


handle_call(_Action, _From, State)  ->  {reply, unknown_action, State}.


handle_cast(stop, State)            ->  {stop, normal, State}.

handle_info(_Action, State)         ->  {noreply, State}.

terminate(_Reason, _State)          ->  ok.
code_change(_OldVsn, State, _Extra) ->  {ok, State}.
