-module(organ_store).
-behaviour(gen_server).

-include_lib("eldap/include/eldap.hrl").
-record(state, {ldap, riak}).

-define(ROOT_DN, "ou=groups," ++ ldap_util:basedn()).
-define(ORGAN_DN(Id), "ou=" ++ Id ++ "," ++ ?ROOT_DN).
-define(BUCKET, <<"cloap.organs.pending">>).


-export([start_link/0]).
-export([init/1, terminate/2, code_change/3,
          handle_call/3, handle_cast/2, handle_info/2]).
-export([create/1, create/2,
            fetch/1, fetch_all/0, update/2, delete/1, name_existed/1, id_existed/1]).
-export([search_organ_members/1, belongs_to/1, add_members/2]).
-export([tree/0]).



% --------------------------------------------------------------------------------
% 【OTP】启动进程
% --------------------------------------------------------------------------------
start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).



% Generate Organ Tree
% --------------------------------------------------------------------------------
tree() ->
    gen_server:call(?MODULE, tree).


% --------------------------------------------------------------------------------
% 【接口】创建机构
% 【描述】根据机构名称创建新的机构，默认无上级机构，并且等级未知
% --------------------------------------------------------------------------------
create(Name) ->
    create(Name, <<>>).
create(Name, Parent)
    when is_binary(Name), is_binary(Parent) ->
    gen_server:call(?MODULE, {create, Name, Parent}).


% 【SEARCH_ORGAN_MEMBERS】查询下级组织机构
% --------------------------------------------------------------------------------
-spec search_organ_members(Id :: string()) -> {ok, [#{}]} | not_found.
search_organ_members(Id) when is_binary(Id) ->
    search_organ_members(binary_to_list(Id));
search_organ_members(Id) ->
    gen_server:call(?MODULE, {search_organ_members, Id}).


% 【BELONGS_TO】查询上级组织机构
% --------------------------------------------------------------------------------
-spec belongs_to(Dn :: string()) -> {ok, #{}} | not_found | root.
belongs_to(Dn) when is_binary(Dn) ->
    belongs_to(binary_to_list(Dn));
belongs_to(Dn) ->
    gen_server:call(?MODULE, {belongs_to, Dn}).



% --------------------------------------------------------------------------------
% 【API】获取全部机构
% --------------------------------------------------------------------------------
fetch_all() -> gen_server:call(?MODULE, fetch_all).

fetch(Id) when is_binary(Id) -> fetch(binary_to_list(Id));
fetch(Id)                    -> gen_server:call(?MODULE, {fetch, Id}).

id_existed(Id) when is_list(Id) -> id_existed(iolist_to_binary(Id)); 
id_existed(Id) when is_binary(Id) -> 
    gen_server:call(?MODULE, {id_existed, Id}).     % 查询 ID 是否在 LDAP 中已经存在

name_existed(Name) when is_binary(Name) -> 
    gen_server:call(?MODULE, {name_existed, Name}).  % 查询名称是否在 LDAP 中已经存在

update(Id, Name) when is_binary(Id) ->
    update(binary_to_list(Id), Name);
update(Id, Name) when is_list(Name) ->
    update(Id, list_to_binary(Name));
update(Id, Name) -> 
    gen_server:call(?MODULE, {update, Id, Name}).

delete(Id) when is_list(Id) ->
    delete(list_to_binary(Id));
delete(Id) -> 
    gen_server:call(?MODULE, {delete, Id}).


add_members(Id, Members) ->
    gen_server:call(?MODULE, {add_members, Id, Members}).


%% 
%% Callbacks
init([]) -> self() ! start_db, {ok, #state{}}.


iter_organ(Graph, Vertex) ->
    {Vertex, L} = digraph:vertex(Graph, Vertex),
    iter_organ_add_members(L, digraph:out_neighbours(Graph, Vertex), Graph).
iter_organ_add_members(Label, [], _Graph) ->
    Label;
iter_organ_add_members(Label, Members, Graph) ->
    Label#{members => [iter_organ(Graph, X) || X <- Members]}.


% --------------------------------------------------------------------------------
handle_call(tree, _From, State = #state{ldap = Ldap, riak = Riak}) ->
    G = digraph:new(),
    V_Root  = digraph:add_vertex(G, <<>>, #{}),

    F_L_Init  = fun(X) ->
        V = digraph:add_vertex(G, id(X#eldap_entry.object_name), entry_to_map(X)),
        digraph:add_edge(G, V_Root, V)
    end,
    F_L_Member = fun(X) ->
        V = id(X#eldap_entry.object_name),
        Members = proplists:get_value("member", X#eldap_entry.attributes),
        [digraph:del_path(G, V_Root, id(E)) || E <- Members],
        [digraph:add_edge(G, V, id(E)) || E <- Members]
    end,
    L_Organs  = ldap_util:search_tree(Ldap, ?ROOT_DN, 
                                    [eldap:equalityMatch("objectClass", "groupOfNames")]),

    F_R_Init  = fun(X = #{id := V}) ->
        V = digraph:add_vertex(G, V, maps:with([id, name], X)),
        digraph:add_edge(G, V_Root, V)
    end,
    R_Organs = riak_util:get_values(Riak, ?BUCKET),

    [ F_L_Init(E) || E <- L_Organs ],
    [ F_R_Init(E) || E <- R_Organs ],
    [ F_L_Member(E) || E <- L_Organs ],
    % [ F_R_Member(E) || E <- R_Organs ],


    Resp = iter_organ(G, V_Root),
    digraph:delete(G),



    {reply, maps:get(members, Resp, []), State};



% 【FETCH_ALL】
% --------------------------------------------------------------------------------
handle_call(fetch_all, _From, State = #state{ldap = Handle, riak = Riak}) ->
    Entries = ldap_util:search_tree(Handle, ?ROOT_DN, 
                                    [eldap:equalityMatch("objectClass", "groupOfNames")]),   
    Ldaps = [ entry_to_map(Entry) || Entry <- Entries ],
    Pends = riak_util:get_values(Riak, ?BUCKET),
    {reply, Ldaps ++ Pends, State};



% 【FETCH】
% --------------------------------------------------------------------------------
handle_call({fetch, Id}, _From, State = #state{ldap = Ldap, riak = Riak}) ->
    Req = #{id => Id},
    Resp = try
        Organs = search_by_id(Id, [Ldap, Riak]),
        ability(id_exist, Id, Organs),
        [Organ] = Organs,
        {ok, Members} = search_organ_members(Id, [Ldap, Riak]),
        {ok, Users}   = search_organ_users(Id, [Ldap]),
        Parent = case search_parent(?ORGAN_DN(Id), [Ldap, Riak]) of
            {ok, V} -> V;
            root    -> <<>>
        end,
        maps:remove(parent, Organ#{members => Members, users => Users, belongs_to => Parent})
    catch
        throw:Reason -> cloap_util:failure(Reason, Req)
    end,
    {reply, Resp, State};





handle_call({id_existed, Id}, _From, State = #state{ldap = Handle}) ->
    {reply, [] =/= ldap_search(Handle, {id, Id}), State};
handle_call({name_existed, Name}, _From, State = #state{ldap = Handle}) ->
    {reply, [] =/= ldap_search(Handle, {name, Name}), State};


% --------------------------------------------------------------------------------
handle_call({create, Name, Parent}, _From, State = #state{ldap = Ldap, riak = Riak}) ->
    Req  = #{name => Name, parent => Parent},
    Resp = try
        ability(name_unique, {Name, Parent}, [Ldap, Riak]),
        Key = cloap_util:uuidgen(binary),
        Organ = Req#{id => Key},
        {ok, Object} = riak_util:set_value(Riak, ?BUCKET, Key, Organ),
        self() ! {add_member, Parent, dn(Key)},
        riak_util:get_value(Object)
    catch
        throw:Reason -> cloap_util:failure(Reason, Req)
    end,
    {reply, Resp, State};



% --------------------------------------------------------------------------------
handle_call({search_organ_members, Id}, _From, State = #state{ldap = Ldap, riak = Riak}) ->
    {ok, Resp} = search_organ_members(Id, [Ldap, Riak]),
    {reply, Resp, State};

% --------------------------------------------------------------------------------
handle_call({belongs_to, Dn}, _From, State = #state{ldap = Ldap, riak = Riak}) ->
    Resp = case search_parent(Dn, [Ldap, Riak]) of
        {ok, R} -> R;
        root -> <<>>
    end,
    {reply, Resp, State};



handle_call({update, Id, Name}, _From, State = #state{ldap = Ldap, riak = Riak}) ->
    Req = #{id => Id, name => Name},
    Parent = search_parent(dn(Id), [Ldap, Riak]),

    Resp = try 
        ability(id_exist, Id, search_by_id(Id, [Ldap, Riak])),
        ability(name_unique, {Name, Parent}, [Ldap, Riak]),

        case Fetched = riak_util:get_value(Riak, ?BUCKET, Id) of
            not_found -> 
                ok = eldap:modify(Ldap, ?ORGAN_DN(Id), [ eldap:mod_replace("cn", [Name])]);
            _ ->
                {ok, _Object} = riak_util:set_value(Riak, ?BUCKET, Id, Fetched#{name := Name})
        end,
        cloap_util:success(Req)
    catch
        throw:Reason -> cloap_util:failure(Reason, Req)
    end,
    {reply, Resp, State};


handle_call({delete, Id}, _From, State = #state{ldap = Ldap, riak = Riak}) ->
    Req = #{id => Id},
    Resp = try 
        ability(id_exist, Id, search_by_id(Id, [Ldap, Riak])),
        case riak_util:get_value(Riak, ?BUCKET, Id) of
            not_found -> eldap:delete(Ldap, ?ORGAN_DN(binary_to_list(Id)));
            _         -> riak_util:remove(Riak, ?BUCKET, Id)
        end,
        cloap_util:success(Req)
    catch
        throw:Reason -> cloap_util:failure(Reason, Req)
    end,
    {reply, Resp, State};

handle_call({add_members, Id, Member}, _From, State = #state{ldap = Ldap})  ->  
    Entries = ldap_search(Ldap, {id, Id}),
    ok = ldap_add_member(Entries, Id, Member, State),
    {reply, cloap_util:success(#{id => Id, member => Member}), State};

handle_call(_Action, _From, State)  ->  {reply, unknown_action, State}.


handle_cast(stop, State)            ->  {stop, normal, State}.


% --------------------------------------------------------------------------------
handle_info({add_member, Parent, Child}, State = #state{ldap = Ldap}) ->
    Entries = ldap_search(Ldap, {id, Parent}),
    ok = ldap_add_member(Entries, Parent, Child, State),
    {noreply, State};

handle_info(start_db, State) ->
    {ok, Handle} = ldap_util:start_db(),
    {ok, Riak} = riak_util:start_db(),
    eldap:add(Handle, ?ROOT_DN, [
                                    {"ou", ["organs"]}, 
                                    {"objectClass", ["organizationalUnit"]}
                                ]),
    {noreply, State#state{ldap = Handle, riak = Riak}};
handle_info(_Action, State)         ->  {noreply, State}.

terminate(_Reason, _State)          ->  ok.
code_change(_OldVsn, State, _Extra) ->  {ok, State}.


% ----------------------------------------------------------------------------------------------------
% 私有函数
% ----------------------------------------------------------------------------------------------------
ldap_search(Handle, {name, Name}) ->
    ldap_search(Handle, "cn", Name);
ldap_search(Handle, {id, Id}) ->
    ldap_search(Handle, "ou", Id).
ldap_search(Handle, Field, Value) ->
    ldap_util:search_tree(Handle, ?ROOT_DN, 
                            [eldap:equalityMatch(Field, Value), eldap:equalityMatch("objectClass", "groupOfNames")]).



% 【PRIV】名称唯一性检测
% --------------------------------------------------------------------------------
ability(name_unique, {Name, Parent}, [Ldap, Riak]) ->
    Nodes = [ N || N <- search_by_name(Name, [Ldap, Riak]), maps:get(parent, N, <<>>) == Parent ],
    cloap_util:demine(fun(X) -> [] == X end, name_existed, Nodes);



% 【PRIV】编号存在性检测
% --------------------------------------------------------------------------------
ability(id_exist, _Id, List) ->
    cloap_util:demine([], not_found, List).



% 【PRIV】按名称查找
% --------------------------------------------------------------------------------
search_by_name(Name, {ldap, Ldap}) ->
    [ entry_to_map(L) || L <- ldap_search(Ldap, {name, Name}) ];
search_by_name(Name, {riak, Riak}) ->
    [ R || R <- riak_util:get_values(Riak, ?BUCKET), maps:get(name, R) == Name ];
search_by_name(Name, [Ldap, Riak]) ->
    search_by_name(Name, {ldap, Ldap}) ++ search_by_name(Name, {riak, Riak}).



% 【PRIV】按编号查找
% --------------------------------------------------------------------------------
search_by_id(Id, {ldap, Ldap}) ->
    [ entry_to_map(L) || L <- ldap_search(Ldap, {id, Id}) ];
search_by_id(Id, {riak, Riak}) ->
    case riak_util:get_value(Riak, ?BUCKET, Id) of
        not_found -> [];
        Fetched   -> [Fetched]
    end;
search_by_id(Id, [Ldap, Riak]) ->
    search_by_id(Id, {ldap, Ldap}) ++ search_by_id(Id, {riak, Riak}).



% --------------------------------------------------------------------------------
% 【PRIV】转换 LDAP 数据项为 MAP
% --------------------------------------------------------------------------------
entry_to_map(Entry) when is_record(Entry, eldap_entry) ->
    maps:from_list(cloap_util:jsonize(
        ldap_util:get_values([ou, cn], Entry), [id, name])).


% --------------------------------------------------------------------------------
% 【PRIV】LDAP 增加成员
% --------------------------------------------------------------------------------
%  节点还在 Riak 数据库中时，直接创建 LDAP 节点，并且删除原 Riak 的数据
% --------------------------------------------------------------------------------
ldap_add_member([], <<>>, _Child, _State) ->
    ok;
ldap_add_member([], Parent, Child, #state{ldap = Ldap, riak = Riak}) ->
    #{name := Name} = riak_util:get_value(Riak, ?BUCKET, Parent),
    ok = eldap:add(Ldap, ?ORGAN_DN(binary_to_list(Parent)), 
            [
                {"objectClass", ["groupOfNames"]},
                {"ou", [Parent]},
                {"cn", [Name]},
                {"member", [Child]}
            ]),
    riak_util:remove(Riak, ?BUCKET, Parent),
    ok;
ldap_add_member([Entry], _Parent, Child, #state{ldap = Ldap}) ->
    ok = eldap:modify(Ldap, Entry#eldap_entry.object_name, [eldap:mod_add("member", [Child])]),
    ok.



% --------------------------------------------------------------------------------
% 【PRIV】【ID】获得唯一值
% --------------------------------------------------------------------------------
-spec id(Dn :: string()) -> Id :: binary().
id(Dn) ->
    Head = iolist_to_binary(re:replace(Dn, "ou=", "")),
    iolist_to_binary(re:replace(Head, "," ++ ?ROOT_DN, "")).
dn(Id) when is_binary(Id) ->
    dn(binary_to_list(Id));
dn(Id) ->
    iolist_to_binary(?ORGAN_DN(Id)).


% --------------------------------------------------------------------------------
search_organ_members(Id, [Ldap, Riak]) ->
    {ok, Members} = ldap_util:get_members(?ORGAN_DN(Id), Ldap),
    LdapChildren = [ maps:put(type, member_type(V), entry_to_map(V)) || {ok, V} <- [ldap_util:get(K, Ldap) || K <- Members ], V =/= not_found, member_type(V) =:= organ ],
    RiakChildren = [ maps:put(type, organ, maps:remove(parent, V)) || V <- lists:filter(fun(X) -> X =/= not_found end, 
        [riak_util:get_value(Riak, ?BUCKET, id(K)) || K <- Members]) ],
    {ok, LdapChildren ++ RiakChildren}.
search_organ_users(Id, [Ldap]) ->
    {ok, Members} = ldap_util:get_members(?ORGAN_DN(Id), Ldap),
    Children = [ maps:put(type, member_type(V), acct_store:entry_to_map(V)) || {ok, V} <- [ldap_util:get(K, Ldap) || K <- Members ], V =/= not_found, member_type(V) =:= user ],
    {ok, Children}.

% ================================================================================
search_parent(Dn, [Ldap, Riak]) ->
    Node            = ldap_util:get(Dn, Ldap),
    {ok, Entries}   = ldap_util:member_of(Dn, ldap_util:basedn(), Ldap),
    Fetched         = riak_util:get_value(Riak, ?BUCKET, id(Dn)),
    parent(Node, Entries, Fetched).
% --------------------------------------------------------------------------------
parent(not_found, [], not_found) ->
    not_found;
parent(not_found, [], _Fetched) ->
    root;
parent(_Node, [], not_found) ->
    root;
parent(_Node, [Entry], _Fetched) ->
    {ok, entry_to_map(Entry)}.



member_type(Entry) ->
    case lists:member("groupOfNames", proplists:get_value("objectClass", Entry#eldap_entry.attributes)) of
        true -> organ;
        false -> user
    end.
