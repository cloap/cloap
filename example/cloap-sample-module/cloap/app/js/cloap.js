'use strict';

angular.module('cloap_sample_app', [
    'ui.router', 'ui.router.state'
])
    .config(function($stateProvider) {
        $stateProvider
            .state("cloap_sample_app", {
                url: "/cloap_sample_app",
                templateUrl: "/cloap/app/cloap_sample_app/partial/index.html",
                controller: function($scope) {
                    $scope.message = "Cloap Sample App.";
                }
            });
    });
