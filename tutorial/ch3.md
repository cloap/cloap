# 第三章 模块开发

本章主要讲述如何在 __CLOAP__ 平台上进行模块开发。

---

## 理解模块

一个模块，就是一个特定的应用。这个应用必须有自己的界面，但却不一定有自己的后台。

这时，我们可以理解 CLOAP 就像一个门户，将不同的应用界面融合在一起，让企业里的使用者不用离开就可以操作全部工作。

在很多时候，我们理解一个企业应用，总是包含了太多太多的东西，开发者会关注：

* 界面展现
* 数据逻辑
* 数据库
* 后台实现

最常见的情况，就是我们在后台通过 JSP、PHP、RAILS 之类的技术或者框架，将数据按预定义的格式组合成界面元素（HTML），然后推送给用户。但这样做面临的困难却有些多。

1. 业务变更时的代码修改点非常多
2. 程序员必须同时掌握前后端开发技巧
3. 不同用户对数据的展现有不同的要求

其实，一言以蔽之，代码过于僵化，而 __“代码复用”__ 被称之为软件开发的圣杯，前后端分离的原因，就在于 __“代码复用”__ ，这大概也是面向服务架构具备强大生命力的原因之一吧。

## 接口说明

### 基本规则

1. 模块必须有一个唯一的应用标识，建议用“ **团队名称**\_ **应用标识** \_module ”格式的名字，如 *makertimes_roster_module*，只能支持字母、下划线与数字，但数字不能是第一个字母
1. 与平台的接口，都是以 /cloap/app 开头，下文中简写为 `APP_BASE`

### 注册信息回调 

* 回调地址：`APP_BASE`/info
* 数据格式：JSON

```javascript
/* 举例 */
{
  "name":  "示例模块",               // 模块的中文名称
  "app_id":  "cloap_sample_app",    // 模块的标识名称
  "release":  "0.0.1",              // 模块的当前版本号
  "released_at":  "2014-10-11",     // 模块的发布日期，注意格式
  "author":  "创客时代工作室",       // 模块的作者描述 
  "descr": "这是 cloap 应用的一个极简模块，他没有具体的功能，但是演示了 cloap 模块所需要的接口",
                                    // 模块的说明
  "settings": { "message": "您好，世界！" }   // 模块的数据配置，请保留字段，但暂时未使用
}
```

### 前端操作回调

* 回调地址： `APP_BASE`/js/cloap.js
* 数据格式： JavaScript

```javascript
/* 举例 */
'use strict';

angular.module('cloap_sample_app', [
    'ui.router', 'ui.router.state'
])
    .config(function($stateProvider) {
        $stateProvider
            .state("cloap_sample_app", {
                url: "/cloap_sample_app",
                templateUrl: "/cloap/app/cloap_sample_app/partial/index.html",
                controller: function($scope) {
                    $scope.message = "Cloap Sample App.";
                }
            });
    });
```

请注意：

1. 必须是 AngularJS 的 Module
1. module、url 一定要与在注册时的 app_id 一致
1. templateUrl 的地址格式，请一定要采用 `APP_BASE`/`APP_ID`/partial/`HTML_FILE`的格式，这是由于平台会实际调用此地址，因此增加了 `APP_ID`
1. 'ui.router' 已经在平台中引入了，无需另外引入
1. 如果有多个JavaScript文件，请打包为一个


### 页面片断回调

* 回调地址： `APP_BASE`/partial/`HTML_FILE`
* 数据格式： HTML5 + CSS3

```html
<!-- 举例 --!>

<h1>Cloap Sample App Message</h1>
<p>{{message}}</p>
```

请注意：

1. 目前 `HTML_FILE` 中不支持目录，只能是文件【下一版本可能改进】
1. 只需要 HTML 片断，请不要植入完整的 HTML 文件
1. HTML片断中可以嵌入一些 CSS 和 JS 代码


## API接口

开发者可以通过 API 接口进行调用。

### 获取全部用户清单

**GET** */cloap/app/role/user/all*

```javascript
/* Example */
[
    {
        "dn": "uid=example,ou=people,dc=soatour,dc=org",   // 用户唯一标识，一般不用
        "login": "example",                                // 用户登录名
        "mail": "example@makertimes.cn",                   // 用户邮箱
        "mobile": "139012345678",                          // 用户手机号
        "name": "张嘎子"                                    // 用户姓名
    },
    ...
]
```


### 获取组织机构分类

**GET** */cloap/app/role/rating/all*

```javascript
/* Example */
[
    {
        "positions": [   // 归口岗位清单
            {
                "dn": "ou=a09f9042-db0b-45e5-96c7-d8bfa7a5c180,ou=positions,dc=soatour,dc=org",
                "id": "a09f9042-db0b-45e5-96c7-d8bfa7a5c180",
                "name": "前端工程师"
            },
            ...
        ],
        "groups": [  // 归口机构清单
            {
                "dn": "ou=xiegang,ou=groups,dc=soatour,dc=org",
                "member": [
                    "uid=xiegang,ou=people,dc=soatour,dc=org"
                ],
                "id": "xiegang",
                "name": "天边工作室"
            },
            ...
        ],
        "dn": "ou=d34ef33b-d1a4-4a84-9840-b2d8cdbb28fd,ou=ratings,dc=soatour,dc=org",  
              // 分类的全路径名称
        "id": "d34ef33b-d1a4-4a84-9840-b2d8cdbb28fd", // 唯一标识
        "name": "工作室"  // 分类名称
    }
]
```


### 获取岗位清单

**GET** */cloap/app/role/position/all*

```javascript
/* Example */
[
    {
        "dn": "ou=a09f9042-db0b-45e5-96c7-d8bfa7a5c180,ou=positions,dc=soatour,dc=org",
        "id": "a09f9042-db0b-45e5-96c7-d8bfa7a5c180",
        "name": "前端工程师"
    },
    ...
]
```

### 获取机构清单

**GET** */cloap/app/role/organ/all*

```javascript
/* Example */

[
    {
        "member": [
            {
                "dn": "uid=example,ou=people,dc=soatour,dc=org",
                "login": "example",
                "mail": "example@zhiyisoft.com",
                "mobile": "",
                "name": "张嘎子"
            },
            ......
        ],
        "dn": "ou=makertimes,ou=groups,dc=soatour,dc=org",
        "id": "makertimes",
        "name": "创客时代工作室"
    },
    ......
]
```


**新增 POST** */cloap/app/role/organ*

```javascript
{
  "name": "组织机构名称",
  "member": ["uid=xxxx,dc=xxx,dc=xxx", ...]
}
```

### 工作分派清单

**GET** */cloap/app/role/job/all*

```javascript
/* Example */
[
    {
        // 哪个部门
        "organ": {   
            "dn": "ou=makertimes,ou=groups,dc=soatour,dc=org",
            "objectClass": "groupOfNames",
            "member": [
                "uid=suyu,ou=people,dc=soatour,dc=org",
                "uid=zhongzhengquan,ou=people,dc=soatour,dc=org",
                "uid=tanghao,ou=people,dc=soatour,dc=org",
                "uid=heyuan,ou=people,dc=soatour,dc=org"
            ],
            "id": "makertimes",
            "name": "创客时代工作室"
        },
        // 哪个岗位 
        "subject": {
            "dn": "ou=884c71a1-4e0b-4bae-b8d7-22e2e23af04f,ou=positions,dc=soatour,dc=org",
            "objectClass": "organizationalUnit",
            "id": "884c71a1-4e0b-4bae-b8d7-22e2e23af04f",
            "name": "主要程序员"
        },
        // 该部门，该岗位，分配给了哪些人员
        "assign": [   
            {
                "dn": "uid=example,ou=people,dc=soatour,dc=org",
                "login": "example",
                "mail": "example@zhiyisoft.com",
                "mobile": "",
                "name": "张嘎子"
            },
            ......
        ],
        "id": "cc500eed-0ce8-40ec-8d49-5ca0506cec7d"
    },
    ....
]
```

