# 第一章 平台环境


本章主要讲述 __CLOAP__ 平台环境的构建及注意事项。

---


## Linux

强烈推荐的 Linux 平台是 __Debian__ ，目前我们支持到 7.0 版本及以上。

如果你只有一个 Windows，那么你可以安装一台虚拟机当作开发环境，然而，对于工作环境，我们建议采用纯粹的 Linux 平台。比如，你完全可以安装在阿里云上。

## Erlang

平台是 Erlang 开发的，当然也需要一个 Erlang 的运行环境，但这也可能不是必须的，请开发者给予支持，编译一个不需要安装 Erlang 环境的包直接发布，___Thanks very much !___

注意，Erlang 的版本必须是 17.0 以上，这是因为我们用到了 __Map__ 语言特性，而更低的版本是不支持的。

## OpenLDAP

我们的用户信息是保留在 OpenLDAP 中的，目前还不支持 AD，但未来会改进到支持 AD。

建议编译安装 OpenLDAP，下面讲述过程，对 LDAP 不了解的，请到网上自行补充基本知识。

### 下载

下载地址在 [OpenLDAP Download](http://www.openldap.org/software/download/) 页面，找到链接地址后，可在 Linux 执行如下操作，以下为举例：

```shell
wget -c ftp://ftp.openldap.org/pub/OpenLDAP/openldap-release/openldap-2.4.40.tgz
```

### 编译安装

在 Debian 下可能需要预先安装一些包，所以你最好提前运行一下：

```shell
apt-get install libdb++-dev libdb-dev libssl-dev g++ gcc make
```

然后就是标准的安装过程：

```shell
./configure
make
sudo make install  # 如果是 root 用户，则不用 sudo 命令
```

### 配置

安装结束后的配置文件在 `/usr/local/etc/openldap/slapd.conf` 中，现在我们需要准备几个参数。

1. 域名，企业的域名，即使没有申请正式的域名也认可，如 `example.org`
2. 根账号，通常以域名为准，如上例，则可写作 `dc=example,dc=org`
3. 管理员账号，一般是在根账号上加上 `cn=admin`，如上例，则为 `cn=admin,dc=example,dc=org`
4. 管理员密码，注意应采用 MD5 或者 SHA1 加密，可以在命令行下用 `echo "passwd" | md5` 生成，注意其中的 _`passwd`_ 请务必修改为自己的口令
5. 修改配置文件，增加相应的 schema 文件和账号信息，如有需要，在 `example` 目录下有参考案例，文件名为 `slapd.conf.example`


### 运行

配置完成后，就可以运行 OpenLDAP。

```shell
/usr/local/libexec/slapd &
```

以上命令将运行 OpenLDAP，但一旦重启则将失效，请将这一句放到 `/etc/rc.local` 文件中。

### 数据初始化

配置还没有结束，我们必须要将根账号加入到数据库中，否则后续工作难以开展，增加的办法是注入一个 LDAP 数据项。

这里举一个例子：

```
dn: dc=example,dc=org
objectClass: dcObject
objectClass: organization
o: example.org 
dc: example.org
```

存为一个文件，例如：`root.ldif` ，然后通过命令将本数据项注入数据库中。

```shell
ldapadd -x -D "cn=admin,dc=example,dc=org" -w passwd -f root.ldif
```

如果提示 ok 则增加成功，至此可以进行下一步。

### CAS Server

【可选项】

单点登录服务器，本处暂略，等待补充。


## CLOAP

我们重点讲解从源代码编译运行安装 CLOAP 的过程，目前这也是推荐的安装方法。

### 下载

```shell
git clone https://git.oschina.net/makertimes/cloap.git
```

### 编译

```shell
cd cloap
./rebar get-deps
./rebar compile
```

### 配置

```shell
cp rel/files/sys.config.example rel/files/sys.config
```

编辑 `rel/files/sys.config` 文件，在后面章节将详细讲解配置参数的设置，目前除 LDAP 的配置外，其余暂且保留默认值。

其中 LDAP 部分的配置，请根据前面 OpenLDAP 的配置信息修改。


### 生成

配置完成后，在 cloap 目录下，运行命令

```shell
./rebar generate
```

平台运行环境则基本完成。

### 数据初始化

平台需要进行数据初始化，在 console 下进行，

```shell
rel/cloap/bin/cloap console

> cloap_store:initdb().
> cloap_store:initdata().
> user_store:init().
```

退出 console 可以按 __Ctrl-C__ 的键盘组合，出现提示后再按 __a__ 键即可回到命令行。

### 运行

cloap 的运行目前很简单，你即可以重新进行 console 环境，也可以在后台启动，后台启动的命令是 start。

```
rel/cloap/bin/cloap start
```

你应该可以猜到，stop 和 restart 命令也是支持的。如同 OpenLDAP，我们也强烈建议将启动命令放到 `/etc/rc.local` 中，注意最好写全路径哟。






