angular.module('cloap_app')
  .directive('smartFormGroup', function($compile, $timeout) {
    return {
        restrict: 'A',
        require: '?ngModel',
        replace: true,
        compile: function(tElem, tAttrs) {
            var currentNode = null;
            var target = tAttrs.target;

            var first = tElem.children().first();
            var last  = tElem.children().last();

            tElem.html(''); // clear

            tElem.attr('class', 'form-group');
            tElem.attr('ng-class', ' \
                        {\'has-error\': ' + target + '.$invalid &&  \
                        (' + target + '.$touched || ' + target + '.$dirty), \
                         \'has-success\': ' + target + '.$valid && \
                        (' + target + '.$touched || ' + target + '.$dirty)}'
                      );

            // label part
            if (tAttrs.label) {
                var required = tAttrs.required === 'true' ? " required" : "";
                tElem.prepend('<label class="control-label' + required + '">' + tAttrs.label + '</label>');
                currentNode = tElem.children().first();
            }

            // input part
            currentNode.after('<div class="input-group"></div>');
            currentNode = tElem.children('div.input-group').first();
            
            if (tAttrs.prefix) {
                currentNode.prepend(
                    '<div class="input-group-addon"><i class="fa fa-fw fa-' + tAttrs.prefix + '"></i></div>'
                );
            }

            first.attr('ng-model-options', "{debounce: {'default': 750, blur: 100}}"); 
            currentNode.append(first);


            // validate part
            currentNode.append(' \
              <div class="input-group-addon" \
                   ng-show="' + target + '.$valid &&  \
                   (' + target + '.$touched || ' + target + '.$dirty)"> \
                <i class="text-success fa fa-check fa-fw"></i> \
              </div> \
              <div class="input-group-addon" \
                   ng-show="' + target + '.$invalid && \
                   (' + target + '.$touched || ' + target + '.$dirty)"> \
                <i class="text-danger fa fa-times fa-fw"></i> \
              </div>');

            // 只在有变更时才显示出错提示
            var last_children = last.children();
            last.html('<div ng-if="' + target + '.$touched || ' + target + '.$dirty"></div>');
            last.children().first().append(last_children);
            tElem.append(last);

            
            $('[data-toggle="tooltip"]').tooltip();

            return {
                pre: function(scope, iElem, iAttrs) {
                },
                post: function(scope, iElem, iAttrs) {
                    iElem.removeAttr('smart-form-group');
                    $compile(iElem)(scope);
                }
            };
        }
    } 
});