class Fixtures
    INVALID_PRODUCT_ID = 0
    NO_RULE_PRODUCT_ID = 1
    VALID_PRODUCT_ID   = 4

    VALID_USER_ID      = 1
    INVALID_USER_ID    = 0
end