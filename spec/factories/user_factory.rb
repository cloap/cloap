class FixtureFactory::UserAccount
    def self.action
        "/users"
    end

    def self.fields
        [[:id, :password, :sn, :cn, :display_name, :mail, :mobile], [:id, :password, :display_name, :mail, :mobile]]
    end

    def self.dtable
        {
            create: [
                [["A000001", nil, "张", "三", nil, "zhangsan@cnpc.com.cn", "13801234567"], ["A000001", nil, "张三", "zhangsan@cnpc.com.cn", "13801234567"]]
            ],
            get: [
                [["A000001"], ["A000001"]]
            ]
        }
    end
end