require_relative '../spec_helper'
require_relative '../factories'


# -------------------------------------------------------------------------------
# 按验收标准进行全程测试
# -------------------------------------------------------------------------------
shared_examples :fixture do
    it "按测试数据组依次测试" do
        reqFields, respFields = factory.fields
        factory.dtable.each do |(req, resp)|
            result = websocket(factory.action, reqFields.zip(req).to_h)
            respFields.zip(resp).each do |(field, value)|
                if (value.respond_to?(:call))
                    expect(value.call(result[field.to_s])).to eq(true)
                else
                    expect(result[field.to_s]).to eq(value)
                end
            end
        end
    end

    def factory
        raise "请在各个测试分组中实现"
    end
end


shared_examples :restful do
    it "新增数据" do
        reqFields, respFields = factory.fields
        factory.dtable[:create].each do |(req, resp)|
            options = reqFields.zip(req).delete_if {|(k,v)| v.nil?}.to_h
            result = httppost(factory.action, options)
            p result
        end
    end

    it "查询数据" do
        reqFields, respFields = factory.fields
        factory.dtable[:create].each do |(req, resp)|
            options = reqFields.zip(req).delete_if {|(k,v)| v.nil?}.to_h
            result = httpget(factory.action + "/#{options[:id]}", options)
            p result
        end
    end


    def factory
        raise "请在各个测试分组中实现"
    end
end


# -------------------------------------------------------------------------------
# 用户账号管理测试
# -------------------------------------------------------------------------------
describe "用户账号管理" do
    include_examples :restful
    def factory 
        FixtureFactory::UserAccount
    end
end